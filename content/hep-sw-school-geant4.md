Date: 2017-06-20
Cathegory: sw-hep-school
Tags: geant4, mc, school, software, tutorials, hep-sem, sci-soft
Authors: Crank
Title: Geant4 Decay Example
Status: draft

Для второй [транссибирской школы по физике высоких энергий](http://hep2019.tpu.ru/)
мы придумали организовать небольшой "виртуальный эксперимент" на Geant4. Формат
школы, к сожалению, не предполагает сколько-нибудь содержательной программы,
в течение которой участникам действительно можно было бы самостоятельно
написать на C++ полноценное приложение, охватив различные аспекты его
архитектуры.

Тем не менее, замысел опирается на тот факт, что заинтересованные участники
смогут почерпнуть более глубокие знания во время небольших мастер-классов, а
простенький интерактивный пример с визуализацией треков частиц в некоторой
простейшей постановке с распадной базой, позволит _всем_, что называется, to
get feeling of how it is done.

# Как это устроено: базовый стэк

Мы подготовили простенький образ для [Oracle Virtual Box](https://www.virtualbox.org/),
содержащий необходимое для работы окружение. Предполагается, что в наш
прогрессивный век у многих, если и нет под рукой Linux, то уж точно найдётся
возможность установить это бесплатное ПО для виртуализации.

Публичная [репа на гитхабе](https://github.com/lyntik/kaondecay) содержит
базовый проект, вполне типичный по структуре подобных приложений:
конфигурирование автосборки посредством CMake, предполагающее наличие в системе
корректно установленных Geant4 и ROOT. Мы приличные люди, и, естественно,
поддерживается out-of-source сборка.

Взаимодействие с ROOT  сцелью визуализации гистограмм делается на Python (т.е.
нужен PyROOT).

# Эксперимент

# Проект

Точка входа вынесена в файл [KaonDecay.cc](https://github.com/lyntik/kaondecay/blob/master/KaonDecay.cc),
лежащий вне основной директории с исходниками. В ней происходят обычные для
подобного сценария операции по предварительному конфигурированию и запуску
основных компонент Geant4:

* Конструируется геометрия детектора -- инстанцируется
[реализация](https://github.com/lyntik/kaondecay/blob/master/include/DetectorConstruction.hh)
интерфейса [`G4VUserDetectorConstruction`](http://www-geant4.kek.jp/Reference/10.05/classG4VUserDetectorConstruction.html));
экземпляр индексируется, понятно, run manager'ом,
* Инстанцируется и регистрируется экземпляр
[закастомленного physics list](https://github.com/lyntik/kaondecay/blob/master/include/PhysicsList.hh) описывающий
происходящую в модели физику,
* Инстанцируется
[реализация](https://github.com/lyntik/kaondecay/blob/master/include/PrimaryGeneratorAction.hh)
primary generator action, задающая точку входа в симуляцию отдельного события,
* Инстанцируется реализация stacking action, нужная для



