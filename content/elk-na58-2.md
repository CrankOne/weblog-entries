Date: 2018-11-16
Cathegory: sw-generic
Tags: common
Authors: Crank
Title: Customizing ELK for User Applications, part II
Status: draft

NOTE: я должен переписать все эти упражнения, поскольку:

- filebeat _не применяет_ ingest pipeline к сообщению. Вместо этого, он
отправляет декларацию pipeline в elasticsearch с тем чтобы уже он затем
применял конвейер
- настроить совместную работу logstash и ingest pipelines [можно](https://www.elastic.co/guide/en/logstash/current/plugins-outputs-elasticsearch.html#plugins-outputs-elasticsearch-pipeline),
но, скорее всего, [избыточно](https://www.elastic.co/blog/should-i-use-logstash-or-elasticsearch-ingest-nodes).


Итак, после небольшой разведывательной вылазки мне удалось скормить ELK большую
часть логов от кастомного софта и убедиться в том что инструмент мне подохдит,
настало время более серьёзных упражнений. Я буду действовать всё в той же
неглубокой ковбойской манере, поскольку я сильно ограничен во времени с этой
работой.

Для начала мне вполне бы хватило писать filebeat'ом напрямую в базу elastic,
как делается в большинстве базовых примеров. Однако по соображениям
безопасности админы CERN запретили мне выставлять открытый порт elasticsearch
даже во внутреннюю сеть, так что теперь мне придётся настраивать цепочку со
звеном logstash: filebeat -> logstash -> elasticsearch.

Я подозреваю, что им просто лень было разбираться в ситуации, поскольку их
автоматический сканер распознавал какие-то cross-site-scripting уязвимости
на порту 9200 restful-интерфейса базы elasticsearch (!). Чушь, но мне не
хотелось строить из себя умника. Пусть будет XSS на REST, замкнём докером
бридж на локалхост, и всё в порядке:

    $ docker run -p 5601:5601 -p 127.0.0.1:9200:9200 -p 5044:5044 -it --name elk sebp/elk

кроме того, конечно, что logstash -- ещё одно звено, добавляющее головняка. Но
люди пишут о каких-то его преимуществах. Учитывая что по-умолчанию контейнер
ELK имеет рудиментарную изоляцию, полагаться на одни лишь iptables рисковано.

# Preface

Я буду рассматривать всё те же процессы CORAL, что и в предыдущей заметке,
однако теперь мне требуется чтобы filebeat парсил и отправлял логи не из
локальной песочницы, а на узлах HTCondor/IBM LSF. Для размещения executables я
буду использовать сетевую шару [AFS](https://en.wikipedia.org/wiki/Andrew_File_System)
предоставленную CERN Computing Infrastructure как общеупотребимое решение. Я не
буду производить на ней никаких блокирующих операций/файлового поллинга и т.д.,
так что читатель в своих рецептах может смело заменить AFS на NFS.

Тут, специально для знакомых с HTCondor нужно оговориться, что концептуально
узлы HTCondor предполагают несколько
иной подход: его развитый scheduling-механизм содержит выразительные средства
описывающие вычислительный цикл с копированием исполняемых файлов и различных
артефактов между локальным submission-узлом и worker nodes при инициализации,
перемещении и завершении процесса. На практике, пользователи обычно предпочитают
этим сложным выразительным средствам сетевые шары. Другими ограничениями
HTCondor, заложенными в его идеологии является изолированность процессов:
никаких или "почти никаких" сетевых соединений, никакого IPC (включая
pipelines, параллельные процессы и шелл-скрипты). По счастью, все эти
ограничения обходятся во "вселенной" `Vanilla`, однако я всё же постараюсь
запускать поменьше процессов и сделать свои сетевые сообщения по возможности
локальными во времени.

Итак, я размещаю распакованный архив filebeat по пути, скажем,
`/afs/user/vendor/filebeat/`. Прокладываю в его поддиректориях (`modules.d/`,
`module/`) симлинки до соответствующих файлов с модулем и его конфигурацией, и
буду запускать filebeat-процесс на ноде HTCondor после того как основной
счётный процесс уже отработал (успешно или нет) и записал свои логи. Этим, я,
конечно, пренебрегаю развитым механизмом polling'а логов filebeat, и несколько
снижу актуальность отображаемой в ELK информации, поскольку log submission
будет производится только после завершения основного процесса, однако так я
останусь в согласии с дизайном HTCondor.

# Тестовые запуски filebeat

## SSL Certificate Issue

Первые тесты на отдельных нодах показали, что логика интеграции filebeat и
logstash из докер-контейнера [sebp/elk](https://elk-docker.readthedocs.io/)
чревата трудностями. Я ожидал, что filebeat сообщит мне об отсутствии
template'ов (документация говорит, что вам нужно
[загрузить их в явном виде](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-template.html#load-template-manually)),
однако вместо этого я столкнулся с проблемами коммуникации beats и logstash
(последний ответственнен за "Lumberjack protocol"):

    $ filebeat ...
    ...
    2018-12-11T14:04:56.900+0100	INFO	pipeline/output.go:95	Connecting to backoff(async(tcp://na58-al-elk.cern.ch:5044))
    2018-12-11T14:04:56.912+0100	INFO	pipeline/output.go:105	Connection to backoff(async(tcp://na58-al-elk.cern.ch:5044)) established
    2018-12-11T14:04:56.982+0100	ERROR	logstash/async.go:256	Failed to publish events caused by: lumberjack protocol error
    2018-12-11T14:04:57.056+0100	ERROR	logstash/async.go:256	Failed to publish events caused by: client is not connected
    2018-12-11T14:04:58.056+0100	ERROR	pipeline/output.go:121	Failed to publish events: client is not connected
    ...
    2018-12-11T14:04:59.147+0100	INFO	pipeline/output.go:95	Connecting to backoff(async(tcp://na58-al-elk.cern.ch:5044))
    2018-12-11T14:04:59.149+0100	INFO	pipeline/output.go:105	Connection to backoff(async(tcp://na58-al-elk.cern.ch:5044)) established
    2018-12-11T14:04:59.237+0100	ERROR	logstash/async.go:256	Failed to publish events caused by: write tcp 188.184.81.142:53540->188.184.85.249:5044: write: broken pipe
    2018-12-11T14:05:00.237+0100	ERROR	pipeline/output.go:121	Failed to publish events: write tcp 188.184.81.142:53540->188.184.85.249:5044: write: broken pipe

или, в дебаге:

    2018-12-11T13:09:29.799+0100	INFO	pipeline/output.go:105	Connection to backoff(async(tcp://na58-al-elk.cern.ch:5044)) established
    2018-12-11T13:09:29.800+0100	INFO	[publish]	pipeline/retry.go:189	retryer: send unwait-signal to consumer
    2018-12-11T13:09:29.800+0100	INFO	[publish]	pipeline/retry.go:191	  done
    2018-12-11T13:09:29.818+0100	DEBUG	[logstash]	logstash/async.go:159	890 events out of 890 events sent to logstash host na58-al-elk.cern.ch:5044. Continue sending
    2018-12-11T13:09:29.835+0100	ERROR	logstash/async.go:256	Failed to publish events caused by: lumberjack protocol error

Есть [открытый тикет](https://github.com/elastic/kibana/issues/25457) с
релевантной темой на GitHub, соданный пару дней назад, и на момент публикации
этой заметки не содержащий комментариев (оставил ссылку на будущее в надежде,
что по ней придут разработчики и объяснят всё терминологически-точно). В это
время [другой тикет](https://github.com/spujadas/elk-docker/issues/24)
и несколько бегло-просмотренных веток обсуждения ошибки
`Failed to publish events caused by: lumberjack protocol error` наталкивают на
мысль что дело в SSL-аутентификации на стороне контейнера. Чтобы проверить это
мне понадобилось немного распотрошить контейнер:

    $ docker exec -it elk bash
    # ls /etc/logstash/conf.d/
    02-beats-input.conf  10-syslog.conf  11-nginx.conf  30-output.conf
    # cat 02-beats-input.conf 
    input {
      beats {
        port => 5044
        ssl => true
        ssl_certificate => "/etc/pki/tls/certs/logstash-beats.crt"
        ssl_key => "/etc/pki/tls/private/logstash-beats.key"
      }
    }

ok, стало быть сертификат для `beats-input` действительно определён в
контейнерах на основе образа `sebp/elk`, я
[проглядел документацию](https://elk-docker.readthedocs.io/#certificates). К
сожалению, этот сертификат выписан на single-part domain name `elk`, а я,
разумеется, не могу редактировать `/etc/hosts` на нодах HTCondor, так что мне
придётся пока отключить SSL-валидацию сервера на время пока не появится
возможность выписать self-signed.

Для работы без SSL достаточно отредактировать
`/etc/logstash/conf.d/02-beats-input.conf`, удалив все поля с префиксом `ssl_`,
и перезапустить сервис `logstash` (в контейнере -- `# service logstash reload`).
Сразу после этого, как ни странно, мои filebeat-процессы оказались способны
заполнять elasticsearch-базу и я немедленно увидел результаты в Kibana:
сообщения из логов хотя и не проходят обработку в ingest pipeline моего модуля,
приходят в elasticsearch и содержат поле `message`.

Для того чтобы сохранить эти изменения, но не коммитить контейнер в образ, я
дополнил запуск контейнера опцией
`-v /opt/02-beats-input.conf:/etc/logstash/conf.d/02-beats-input.conf`.

## Поля и некоторые дополнительные приготовления

В своей первой заметке я работал с
[репозиторием beats](https://github.com/elastic/beats.git) для создания своего
модуля и fileset'а к нему. Хотя при помощи создания символических ссылок в
директорию с распакованным filebeat, мне и удалось использовать магию beats
для синхронизации маппинга полей (я даже не заметил, как это произошло),
ситуация с включённым в цепочку logstash сложнее: мне
[понадобится](https://discuss.elastic.co/t/new-module-fields-yml-and-filebeat-template-json/95352)
теперь сгенерировать и дополнить список полей для модуля, и затем вручную
загрузить шаблоны индексов в elasticsearch.

Именно, в своих предыдущих упражнениях я проигнорировал команды `make filds` и
`make update`, ответственные за генерацию индекса полей. Первая команда для
случая моих моим Grok templates в ingest pipeline coral/log выглядит так:

    $ make create-fields MODULE=coral FILESET=log

Результатом её является файл `module/coral/log/_meta/fields.yml`, в котором
следует затем ещё вручную заполнять поля документации: `description`/`example`
(будте аккуратны, `make create-fields` молча затрёт все изменения в этом файле
после неосторожного перезапуска).

Для `make update` создающего затем, собственно, `index pattern` модуля
необходима (локальная) установка golang, содержащая [mage](https://magefile.org/)
и [pkg/errors](https://github.com/pkg/errors):

    $ export GOPATH=~/go
    $ go get -u -d github.com/magefile/mage
    $ cd $GOPATH/src/github.com/magefile/mage
    $ go run bootstrap.go
    $ go get github.com/pkg/errors

После этого:

    $ PATH=$PATH:~/go/bin make update
    
Создаст `filebeat/_meta/kibana.generated/6/index-pattern/filebeat.json`,
содержащий обновлённый индекс.

Тонкость на которую я потратил некоторое время: Makefile содержит шелл-скрипт
внутри выполняющий проход по дереву директорий внутри `modules/`. Этот клочок
скрипта оболочки, очевидно, где-то проверяет тип записи в дереве, и включает
_только директории_ (но не символические ссылки!). Т.е., симлинк на какую-то
директорию снаружи попросту будет проигнорирован, и это было критическим
попаданием для меня, поскольку свой модуль я хранил в git-проекте снаружи.

Поскольку я ничерта не понимаю в экосистеме Go, некоторые из шагов выше могут
быть избыточны.

    $ make
    $ curl -XGET 'localhost:9200'
    $ ./filebeat export template | grep coral
    $ ./filebeat export template > ./filebeat.template.json
    $ ./filebeat ... -M "coral.log.var.paths=[/afs/cern.ch/work/r/rdusaev/na58/al.ws/alignment/P05phys2/traf.285962/traf.P05phys2.0/logs/11.out.txt]"

## Adding Indexes

В ELK существуют шаблоны индексов
([index templates](https://www.elastic.co/guide/en/elasticsearch/reference/6.5/indices-templates.html)),
которые определяют, собственно, структуру сообщения (документа) хранимого в
базе. Т.е., в известном смысле эти шаблоны соответствуют схемам в обычных
реляционных БД. Есть, конечно, возможность определять их взаимодействуя с
elasticsearch-базой посредством RESTful API, но filebeat, разумеется, может это
сделать за вас. Однако, если elasticsearch делает это автоматически напрямую,
то с посредничеством logstash вам следует
[сделать это вручную](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-template.html#load-template-manually):

    $ ./filebeat setup --template -E output.logstash.enabled=false \
                        -E 'output.elasticsearch.hosts=["localhost:9200"]'

В принципе, эта инструкция просто выключает logstash-output и производит
синхрониацию шаблонов индексов так как если бы filebeat общался с базой
напрямую (или вернее сказать, что setup stage это часть обычной
последовательности работы beats без посредничества logstash?).

Разумеется, поскольку мой стек допускает операции с elasticsearch только на
стороне хоста docker-контейнера, я должен либо запускать эту команду там, либо
экспортировать шаблоны индексов в `.json` и добавлять затем их вручную
используя REST-интерфейс elasticsearch.

# Изменения в конфигурации filebeat

Я оставил дефолтный файл `filebeat.yml` в корне дистрибутива и написал свой,
в котором в качестве `output` указан только logstash. Вот его полная форма на
данный момент:

    filebeat.inputs:
    - type: log
      enabled: True
      paths:
          - ./*-coral.out
          - ./*-coral.err
      fields:
        runTag: null  # TODO: change

    filebeat.config.modules:
      path: ${path.config}/modules.d/coral.yml
      reload.enabled: false

    setup.template.settings:
      index.number_of_shards: 3

    name: TestShipper  # TODO: change
    tags: [ "coral" ]

    setup.kibana:
      host: "na58-al-elk.cern.ch:5601"

    output.logstash:
      hosts: ["na58-al-elk.cern.ch:5044"]

    processors:
      - add_host_metadata: ~                                                        
      - add_cloud_metadata: ~

Я планирую запускать filebeat на рабочих нодах в скрипте оболочки, откуда
filebeat будет брать метаданные, а параметры `name`/`tags` я надеюсь
перегружать из командной строки (`-E`,
[см. напр.](https://www.elastic.co/guide/en/beats/filebeat/current/command-line-options.html#global-flags)):

    filebeat --modules coral -e -c ${FBD}/filebeat.yml -E "name=myProcess" \
        -E "path.data=$(readlink -f beats-data/)" \
        -E "path.logs=$(readlink -f beats-data/)"

TODO: Я пока не вполне понимаю, как можно было бы выставить
`filebeat.inputs[0].fields.runTag`, однако оставлю пока этот вопрос.

Если filebeat executables расположены на сетевой шаре, необходимо
переопределить как минимум путь `data` для того чтобы filebeat разместил там
свой реестр в котором хранится информация об открытых файлах и источниках
логов, иначе вы столкнётесь с очевидными коллизиями:

    ERROR	instance/beat.go:800	Exiting: Can only start an input when all
    related states are finished: {Id:XXXXXXX-XXXXX Finished ... }

Также я переопределил `path.logs` поскольку собственные логи filebeat сами по
себе мне будут малоинтересны -- после завершения filebeat я делаю дамп
содержимого этой директории в `stdout`-лог HTCondor'а.

## Дополнительные возможности Пятачка

Из C++-кода CORAL я выудил следующую информацию (заметки о конкретных местах
сделаны по большей части для себя):

* Возможные токены в поле `Severity`: 
    * `DEBUG` -- "Max information level"
    * `VERBOSE` -- "More infos that the default"
    * `INFO` -- "Normal procedural messages"
    * `ANOMALY` -- "Something anomalous. But understood..."
    * `WARNING` -- "Some unexpected behaviour. Result could be wrong."
    * `ERROR` -- "Requests or action not produced. Severe."
    * `FATAL` -- "No way to continue. The program must end."
    * `NOTA BENE` -- "Mandatory info"
* Помимо полновесного многострочного формата с полными именами полей "Date",
"File/Facility", есть и ещё один, вида:
    \n<severity>, on <date>\nfrom: <file>   <line>\n`<msg>'
который, видимо, сделан, чтобы быть более лаконичным. (управляется параметром
`verbosity` = [ `LOW`, `MEDIUM`, `HIGH` ], отвечающим только за данные варианты
форматирования сообщений лога).
* В самой реализации логгера нет возможности писать в файл -- единственная
доступная крутилка сохранения лога изнутри CORAL (`store level`) устанавливает
минимальный приоритет сообщения (`INFO`/`ANOMALY`/etc.), чтобы при аномальном
завершении процесса напечатать всё в `stdout`.
