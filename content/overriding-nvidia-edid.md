Date: 2016-02-11
Category: sw-generic
Tags: geant4, mc, physics, dark matter
Authors: Crank
Title: Overriding Monitor EDID with proprietary NVidia drivers
Status: draft

**Проблема**: `nvidia-settings` сообщает

## Как получить EDID?

В случае проприетарных драйверов от nvidia, дело решается кнопочкой
«Get EDID» в секции соответствующего монитора утилиты `nvidia-settings`.

В общем же,
есть несколько различных путей и специализированных утилит для этой
цели, но самое простое — считать EDID‐string командой

    $ xrandr --props

Если, однако, понадобится работать с EDID‐информацией, и потому было бы
целесообразно установить профильный пакет `read-edid` для этих целей.
В Gentoo ебилд есть в официальном дереве portages `x11-misc/read-edid`.

## How to override EDID info?

В секции `"Device"` `xorg.conf` оказывается возможно специфицировать
кастомный EDID‐файл. Примерно так:

    Option         "CustomEDID" "DFP-0:/root/edid.bin"
    Option         "IgnoreEDID" "false"
    Option         "UseEDID"    "true"

[EDID\_Conver](terhttp://www.komeil.com/download/2750)

