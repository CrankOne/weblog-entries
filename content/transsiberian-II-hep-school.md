Date: 2017-06-20
Cathegory: sw-hep-school
Tags: geant4, mc, school, software, tutorials, hep-sem, sci-soft
Authors: Crank
Title: Geant4 Excercise for Transsiberian High Energy School
Status: draft

# Rationale

The proposed practical exercise implies the observation of short-living
particle decay by it's footprint in dense calorimeter covering some acceptance
over lengthy decay volume.

Such a simplistic experiment shall give a brief gist of how the most of the
short-lived particle are observed in high energy physics by their
decay products with longer lifetime and (or) greater cross-section.

Typical data treatment pipeline usually exploits measured values of
energy deposition and track footprint to reconstruct the momenta of particular
products. That is where various detector roles in real experiments are coming
from (trackers, calorimeters, etc). Reproducing of all the steps is not
necessary for general understanding of the process, however.

![Single particle case.]({static}images/articles/transsiberian-II-hep-school/kinematics_drawing.svg){.figure .center-align}

With some imaginary experimental fixture implemented by means of Monte-Carlo
probabilistic simulation approach we offer to perform the last stage of
analysis, having all the useful information about long-living products
coming from decay volume to sensitive volume. It shall be possible to
reconstruct the primary vertex within the decay volume, derive the lifetime and
yield with key-point assumptions about the nature of initial particle.

# Getting Started

The software part of MC simulations (interfacing with Geant4 by C++) is
isolated from user in our [virtual machine bundle](https://drive.google.com/file/d/1Lx82TDIm7cNsDYONaifAUDXDiLV6a5kF/view?usp=sharing).

## An Overview

By clicking on the icon of example one get into the Geant4 Qt GUI, where
initial actions might be taken in order to have closer look on how the setup
actually looks like.

![Screenshot of app's GUI.]({filename}images/articles/transsiberian-II-hep-school/run-example-icon.png){.figure .center-align}

By running

    /run/beamOn 10

one may generate few events, observing the track visualization over detector
and decay volumes.

![Screenshot of app's GUI.]({filename}images/articles/transsiberian-II-hep-school/example-gui.png){.figure .center-align}

We choose our calorimeters being of horseshoe-like form to accomplish the
uniform phi-acceptance coverage with respect to "beam" axis. This have to
simplify vertexing process further.

## Simulations in Batch mode

In order to get significant statistics, the app have to be ran in batch mode.
Open terminal, get to the folder with application deployment and run the
script:

    $ cd ~/Projects/g4decay/build
    $ ./batch-run.sh

This will generate 1e4 events and write out the events output statistics into
`histo.root` and `tree.root` in the same dir. The latter contains couple
of histograms, one may validate their analysis with, while former have all the
simulated data written.

![Screenshot of app's GUI.]({filename}images/articles/transsiberian-II-hep-school/events-tbrowser.png){.figure .center-align}

They are now available for browsing. One may explore their content by means pf
ROOT TBrowser for reference.

## Analysis

We offer the Python Jupyter notebook environment in order to make this toy
analysis faster and more efficient comparing to traditional scripting tools.
Since this approach usually demands users to carefully maintain the code we've
pre-assembled some basic routines for the further exercises.

To see how the iteration among the recorded events is done, refer to
`printEvents.py` script (this is native Python script, not a Jupyter
notebook!).

To run the Jupyter, enter:

    $ ./run-jPy.sh

The script will raise up a Python evaluation kernel together with notebook
session available for input.

![Screenshot of app's GUI.]({filename}images/articles/transsiberian-II-hep-school/jupyter-look.png){.figure .center-align}

Working under the Python environment might be somewhat tricky for beginners,
especially concerning the ROOT bindings and hence it is recommended either
to stick to native Python structures, or operate with pre-defined snippets that
might be found on the same dir.

# Decay Kinematics

Spatial vertex reconstruction relies one the intersection of the decay product
tracks. Since in our setup we operate mostly in simple non-branching decay
mode (unknown particle decays into couple of leptons), there is only one
(primary) vertex in analysis that might be located based on sensitive volume
intersection points and momenta directions. One may immediately rely on this
information to derive the unknown particle lifetime by time-of-flight approach,
or use energy deposition data to find out the particular decay product types
to build up a narrower analysis.

![Single particle case.]({filename}images/articles/transsiberian-II-hep-school/kinematics_drawing_1.svg){.figure .center-align}

Since here we operate with set built only on complanar vectors one can
effectively get rid of one geometrical dimension by orienting 2D coordinate
frame ($Y$, $Z$) within the plane of decay.

## Geometry

To build up a vertex reconstruction procedure, one may consider
momenta $\vec{p}\_0$, $\vec{p}\_1$ derived from signal within some reactive
media (detectors) at the intersection points $\vec{r}\_0$ and $\vec{r}\_1$.
This values are bound together at the vertex point $\vec{r}$ by simple
geometrical condition of point being common for both track lines.

Denoting the unit vector of $\vec{p}$ as $\hat{\vec{p}}$ one may express the
stright line of decay product track as:

\begin{equation}
\frac{r\_z - r\_{0z}}{\hat{p}\_{0z}} = \frac{r\_y - r\_{0y}}{\hat{p}\_{0y}}
\end{equation}

In case when parametric units aren't important, one can safely get rid of
denominator in unit vectors component, e.g.:
$\hat{p}\_{0z} = \vec{p}\_{0}/|\vec{p}\_{0z}|$.

By writing similar equation for second intersection point ($\vec{r}\_1$) the
vertex coordinates may be expressed in a strightforward way:

\begin{equation}
r\_z = ( p\_{0y} p\_{1z} - p\_{0z} p\_{1y} )^{-1}
( - p\_{0z} p\_{1y} r\_{1z} + p\_{0z} p\_{1z} (r\_{1y} - r\_{0y}) + p\_{0y} p\_{1z} r\_{0z} ), \\\\
r\_y = \frac{p\_{1y}}{p\_{1z}} (r\_z r\_{1z}) + r\_{1y}.
\end{equation}

The $\vec{p}\_0$ momentum may be either known *a priori* (e.g. defined by
some emitting apparatus -- accelerator or magnetic field) or derived
*a posteriori* from other shoulder of detecting goniometer.

![Double particle case.]({filename}images/articles/transsiberian-II-hep-school/kinematics_drawing_2.svg){.figure .center-align}

Besides of this two isolated cases one may build up the vertex reconstruction
routine using redundant information to sharpen the numbers.

The error estimation is the key part of the propsed practice excercises. All
the possible sources of various errors are immersed into our simulation
pipeline. For the sake of clarity we've made all this sources controllable
(through the `.mac` scripts):

* Decay products may suffer from scattering on air filling the decay volume
* The energy of the source particle as well as it's direction may slightly vary
as it usually happens in real experiments
* Values related to the decay products that are "measured" by our idealistic
"detector" are initially taken per se. However it is a Pb calorimeter and one
could rely on it's real energy deposition leading to significant (but more
realistic) imprecise.

# Questionnaire

In real experiments it is usually found to be a trade-off between the
engineering & technical limitations, particle source luminosity and data
precision. By thinking on the following questions one may get a brief idea on
these conditions:

* What is the most prominent limiting factor for the precision of reconstructed
vertex? How one should elaborate the setup to gain more precise results?
* How this imaginary decay experiment corresponds to the typical fixed target
or collider setups?
* In case of branching process, how one shall elaborate the setup to catch the
secondary vertexes?

There are few pre-defined routines to estimate the measured lifetime
error *a priori*, as indirect measurement errors and *a posteriori*, as a
standard deviation from fitted value. By getting closer to realistic condition,
one may observe increasing discrepancy between this two estimations, and thus
sort out the most contributing factors.

